<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Деловое общение, переговоры!</title>
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/meyer-reset/2.0/reset.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<img src="img/bg.jpg" alt="" class="body-bg"/>

<div class="main">
    <h2>Деловое общение, переговоры?</h2>
    <h1>Легко</h1>
    <h3>Используйте <span>10 фишек</span> и уверенно <span>получите результат!</span></h3>

    <div class="wrap">
        <div class="form">
            <div class="big-ico"></div>
            <p class="form-text">Чтобы получить чек-лист, <br/> введите свои данные: </p>

            <?php include 'form.php'; ?>

       		<p class="form-bot-text">100% гарантия <br/> конфиденциальности</p>
        </div>
        <div class="clear"></div>
        <script type="text/javascript">(function() {
            if (window.pluso)if (typeof window.pluso.start == "function") return;
            if (window.ifpluso==undefined) { window.ifpluso = 1;
                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                var h=d[g]('body')[0];
                h.appendChild(s);
            }})();</script>
        <div class="pluso" data-background="transparent" data-options="big,square,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
        <div class="clear"></div>
    </div>
</div>

<div class="footer">
    <p>Все права защищены. Copyright © 2014. Дмитрий Иванов</p>
</div>

<script src="http://cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>